# Skippr Output Plugin CDK

Base classes and stubs to assist development and testing in IDE's.

# Local testing


```bash

# Build container with test suite and libs                
docker build --target devbuild -t output-test:latest . 
                      
# Build final container to test
docker build --target final -t output-build:latest .
                                          
# Run integration tests
docker run --privileged -v /var/run/docker.sock:/var/run/docker.sock -v $PWD/test-data:/usr/src/app/src/Skipprd/Plugins/DataOutputs/Kafka/test-data -e HOST_PATH=$PWD/test-data output-test:latest composer phpunit

```

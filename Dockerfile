FROM skippr/skipprd:v1.2.14 AS install
#FROM skipprd:build AS install

ARG DEBIAN_FRONTEND=noninteractive

##
# Install any required system libs
##

##
# Dev Build plugin
##

FROM install as devbuild

WORKDIR /usr/src/app/src/Skipprd/Plugins/DataOutputs/${PLUGIN_NAME}

RUN apt-get update -y \
    && apt-get install -y \
    docker.io \
    && rm -rf /var/lib/apt/lists/*

RUN wget -O composer-setup.php https://getcomposer.org/installer \
  && sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
  && rm composer-setup.php

COPY . .

RUN composer install --no-scripts --no-interaction --no-ansi --prefer-dist --no-progress --optimize-autoloader

WORKDIR /usr/src/app

RUN composer dumpautoload

WORKDIR /usr/src/app/src/Skipprd/Plugins/DataOutputs/${PLUGIN_NAME}


##
# Prod Build plugin
##
FROM install as build

WORKDIR /usr/src/app/src/Skipprd/Plugins/DataOutputs/${PLUGIN_NAME}

RUN wget -O composer-setup.php https://getcomposer.org/installer \
  && sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
  && rm composer-setup.php

COPY . .

RUN composer install --no-dev --no-scripts --no-interaction --no-ansi --prefer-dist --no-progress --optimize-autoloader

WORKDIR /usr/src/app

RUN composer dumpautoload


##
# Final
##
FROM install as final

WORKDIR /usr/src/app/src/Skipprd/Plugins/DataOutputs/${PLUGIN_NAME}

COPY --from=build /usr/src/app/src/Skipprd/Plugins/DataOutputs/${PLUGIN_NAME}/src .

WORKDIR /usr/src/app

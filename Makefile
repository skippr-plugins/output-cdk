PLUGIN_NAME=File

all: install lint check test


install:
	@composer install; \
    composer cghooks update # https://github.com/BrainMaestro/composer-git-hooks#adding-hooks

lint:
	@composer phpcbf

check:
	@composer phpstan; \
	composer phpcs

test:
	@docker build --target devbuild --build-arg PLUGIN_NAME=$(PLUGIN_NAME) -t output-test:latest . \
	&& docker build --target final --build-arg PLUGIN_NAME=$(PLUGIN_NAME) -t output-build:latest . \
	&& docker network create --driver bridge proxynet \
	; docker-compose up -d --remove-orphans \
    && sleep 30 \
	&& docker run --privileged --network=proxynet -v /var/run/docker.sock:/var/run/docker.sock -v $(PWD)/test-data:/usr/src/app/src/Skipprd/Plugins/DataOutputs/$(PLUGIN_NAME)/test-data -e HOST_PATH=$(PWD)/test-data output-test:latest composer phpunit \
	; docker-compose down

# used to ensure githooks work in Source Tree, but opening it from out current DIR
openst:
	@open /Applications/Sourcetree.app
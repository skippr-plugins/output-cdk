<?php

namespace Skipprd\Plugins\OffsetDrivers;

interface OffsetDriverInterface
{

    public function get() : array;

    public function sync(string $namespace, string $partition, string $offset) : void;

//    public function syncAll(array $offsets) : void;
}

<?php

namespace Skipprd\Traits;

class Config
{

    public static $batchFormats = [
        'parquet',
        'csv',
        'xml',
        'avro_file'
    ];
}

<?php

namespace Tests\Integration;

use Docker\API\Endpoint\VolumeCreate;
use Docker\API\Model\HostConfig;
use Docker\API\Model\HostConfigLogConfig;
use Docker\API\Model\Volume;
use Docker\API\Model\VolumesCreatePostBody;
use Tests\DockerRun;
use PHPUnit\Framework\TestCase;
use Docker\API\Model\ContainersCreatePostBody;
use Docker\Docker;

class CompletenessTest extends DockerRun
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testIngestCompleteness()
    {

        $this->testFile = '100.json.gz';

        $envs = [
            'DATA_OUTPUT_PLUGIN_NAME=file',
            'DATA_OUTPUT_PATH=/data/input',
            'DATA_OUTPUT_FORMAT=parquet',
            'DATA_DIR=/data',
            'TENANT_ID=skippr',
            'PIPELINE_NAME=uattest',
            'ANONYMOUS_METRICS=false',
            'LICENSE_KEY=94708298-498f-4c74-802c-ff359dd56cdf',
            'APP_ENV=dev',
        ];

        $this->containerConfig->setEnv($envs);

        $this->dockerRun();

        $this->assertParquetOutput();
    }
}
